import telebot
from telebot.types import InlineKeyboardMarkup, InlineKeyboardButton
from loguru import logger
from sqlalchemy import select

import asyncio

from companent.config import TG_API_KEY
from companent.const import start_png, ref_png
from companent.sub_def import check, reflink, back, runFiai
from companent.structures import StartButtom
from companent.db import get_async_session
from companent.models import UserSD

bot = telebot.TeleBot(TG_API_KEY)
logger.add("log/bot.log", rotation="500 MB")


async def user_add(user_info: StartButtom):
    async with get_async_session() as session:
        user_id = int(user_info.get("id"))
        ref_id = user_info.get("refid").replace("/start ", "") if user_info.get("refid") else "default_ref_id"
        if ref_id.isdigit():
            ref_id = int(ref_id)
        else:
            ref_id = None

        user_db_requests = select(UserSD).where(UserSD.userid == user_id)
        user_db = await session.scalar(user_db_requests)
        logger.debug(f"{user_info}")

        new_user_data = {
            "userid": user_id,
            "refid": f"{ref_id}" if ref_id and ref_id != user_id else None,
            "souls": 0,
            "ref": 0,
            "name_user": f'{user_info.get("first_name")} {user_info.get("last_name")}',
            "name_user_tg": user_info.get("username"),
        }
        if not user_db:
            new_user = UserSD(**new_user_data)
            session.add(new_user)
            if ref_id and ref_id != user_id:
                try:
                    user_ref_db_requests = select(UserSD).where(UserSD.userid == ref_id)
                    user_ref_db = await session.scalar(user_ref_db_requests)
                    if user_ref_db:
                        user_ref_db.souls += 1
                        user_ref_db.ref += 1
                        souls = user_ref_db.souls
                        bot.send_photo(
                            ref_id,
                            caption=f"Тебе повезло! Благодаря твоей ссылке к нам присоединился новый Новобранец, а тебе начислена одна душа.\nТекущее количество душ: {souls}",
                            photo=ref_png,
                        )
                except Exception as e:
                    logger.error(f"Ошибка при обновлении данных реферера: {e}")
            await session.commit()


@bot.message_handler(commands=["start"])
def start(
    message,
):
    requests_user = message.from_user.to_dict()
    requests_user["refid"] = str(message.text.replace("/start ", ""))
    user = StartButtom(**requests_user)

    asyncio.run(user_add(user))

    markupCheck = InlineKeyboardMarkup()
    check_but = InlineKeyboardButton(text="Готово!", callback_data="check_but")
    markupCheck.add(check_but)
    chat_id = message.chat.id
    first_name = message.chat.first_name
    bot.send_photo(
        chat_id,
        photo=start_png,
        caption=f"Привет {first_name}\nТы на верном пути, Новобранец! Осталось открыть эту дверь, а для этого необходимо подписаться на канал @strashnoe_delo. Как только подпишися - возвращайся, здесь тебя будет ждать открытая дверь.",
        reply_markup=markupCheck,
    )


@bot.message_handler(content_types=["text"])
def sols_toty(message):
    admin = [405432527, 848702189]
    if message.from_user.id in admin:
        try:
            keys, id_us, souls = message.text.split(" ")
            souls = int(souls)
            if id_us.isdigit():
                if keys == "т":
                    if db.user_exists(id_us):
                        db.plus_souls_toty(id_us, souls)
                        soul = db.count_souls(id_us)
                        bot.send_message(message.chat.id, "Души начислены")
                        bot.send_message(id_us, f"Начислены души за тату\nТекущее количество душ: {soul}")
                        if db.chel_ref(id_us) != "-1":
                            bot.send_photo(
                                db.chel_ref(id_us),
                                caption="Приглашенный вами Новобранец сделал татуировку, вам начислена 1 душа",
                                photo=ref_png,
                            )
                        else:
                            pass

                    else:
                        bot.send_message(message.chat.id, "Человек не пользу ботом")
                elif keys == "к":
                    if db.user_exists(id_us):
                        db.plus_souls_toty(id_us, souls)
                        soul = db.count_souls(id_us)
                        bot.send_message(message.chat.id, "Души начислены")
                        bot.send_message(id_us, f"Начислены души за конкурс\nТекущее количество душ: {soul}")
                    else:
                        bot.send_message(message.chat.id, "Человек не пользу ботом")

            else:
                bot.send_message(message.chat.id, "Ведите id не верно")
        except:
            bot.send_message(message.chat.id, "Введино не в верной формате")
    else:
        pass


@bot.callback_query_handler(func=lambda call: True)
def callback(call):
    if call.message:
        if call.data == "check_but":
            check(call)
        elif call.data == "work_button":
            reflink(call)
        elif call.data == "back_button":
            back(call)
        elif call.data == "run_button":
            runFiai(call)
        elif call.data == "run_back":
            back(call)


bot.infinity_polling()
