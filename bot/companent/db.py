from sqlalchemy.pool import NullPool
from sqlalchemy.ext.asyncio import AsyncSession, create_async_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from contextlib import asynccontextmanager

from companent.config import SQLALCHEMY_ASYNC_DATABASE_URL

async_engine = create_async_engine(
    SQLALCHEMY_ASYNC_DATABASE_URL,
    echo=True,
    future=True,
    poolclass=NullPool,
)

async_session_maker = sessionmaker(async_engine, class_=AsyncSession, expire_on_commit=False, autoflush=True)


@asynccontextmanager
async def get_async_session() -> AsyncSession:
    async with async_session_maker() as session:
        yield session

Base = declarative_base()