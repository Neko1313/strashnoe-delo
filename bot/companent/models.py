from sqlalchemy.orm import Mapped, mapped_column, relationship
from sqlalchemy import Integer, String, Text, Boolean, ForeignKey

from companent.db import Base


class Item(Base):
    __tablename__ = "main_item"
    id: Mapped[int] = mapped_column(Integer, primary_key=True)
    price: Mapped[int] = mapped_column(Integer, nullable=False)
    info: Mapped[str] = mapped_column(Text, nullable=False)
    name: Mapped[str] = mapped_column(String(30), nullable=False)
    img: Mapped[str] = mapped_column(String, nullable=False)  # Путь к изображению в виде строки
    is_limit: Mapped[bool] = mapped_column(Boolean, nullable=False, default=False)
    limit: Mapped[int] = mapped_column(Integer, nullable=True)

    def __repr__(self):
        return f"<Item(name={self.name}, price={self.price})>"


class UserSD(Base):
    __tablename__ = "main_usersd"
    id: Mapped[int] = mapped_column(Integer, primary_key=True)
    userid: Mapped[int] = mapped_column(Integer, nullable=True, unique=True)
    refid: Mapped[str] = mapped_column(String(30), nullable=False)
    souls: Mapped[int] = mapped_column(Integer, nullable=False)
    ref: Mapped[int] = mapped_column(Integer, nullable=False)
    name_user: Mapped[str] = mapped_column(Text, nullable=False)
    name_user_tg: Mapped[str] = mapped_column(Text, nullable=True)

    def __repr__(self):
        return f"<UserSD(userid={self.userid}, name_user={self.name_user})>"
    
    def add_user(self):
        pass


class ItemsUser(Base):
    __tablename__ = "main_itemsuser"
    id: Mapped[int] = mapped_column(Integer, primary_key=True)
    userid: Mapped[int] = mapped_column(Integer, ForeignKey("main_usersd.id"), nullable=False)
    itemid: Mapped[int] = mapped_column(Integer, ForeignKey("main_item.id"), nullable=False)

    user: Mapped["UserSD"] = relationship("UserSD", back_populates="purchased_items")
    item: Mapped["Item"] = relationship("Item", back_populates="buyers")

    def __repr__(self):
        return f"<ItemsUser(userid={self.userid}, itemid={self.itemid})>"


UserSD.purchased_items = relationship("ItemsUser", back_populates="user")
Item.buyers = relationship("ItemsUser", back_populates="item")
