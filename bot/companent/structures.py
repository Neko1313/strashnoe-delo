from typing import TypedDict


class StartButtom(TypedDict):
    id: int
    is_bot: bool
    first_name: str
    username: str
    last_name: str
    language_code: str
    can_join_groups: str | None | bool
    can_read_all_group_messages: str | None | bool
    supports_inline_queries: str | None | bool
    is_premium: None | bool
    added_to_attachment_menu: str | None | bool
    refid: str | None
