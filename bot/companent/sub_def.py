from telebot.types import InputMediaPhoto, InlineKeyboardMarkup, InlineKeyboardButton

import importlib

from companent.config import BOT_NICKNAME, CHANNEL_ID
from companent.const import profil, page2, run_png

async def get_user(telegram_id):
    pass

def runFiai(call):
    bot = importlib.import_module("main.bot")
    markupRun = InlineKeyboardMarkup()
    run_back = InlineKeyboardButton(text="Вернуться назад", callback_data="run_back")
    markupRun.add(run_back)
    bot.edit_message_media(
        media=InputMediaPhoto(run_png), chat_id=call.message.chat.id, message_id=call.message.message_id
    )
    bot.edit_message_caption(
        chat_id=call.message.chat.id,
        message_id=call.message.id,
        caption="Бежать здесь некуда",
        parse_mode="MarkdownV2",
        reply_markup=markupRun,
    )


def reflink(call):
    bot = importlib.import_module("main.bot")
    link = f"`https://t.me/{BOT_NICKNAME}?start={call.message.chat.id}`"
    ref = db.count_reefefels(call.message.chat.id)
    souls = db.count_souls(call.message.chat.id)
    markupBack = InlineKeyboardMarkup()
    back_button = InlineKeyboardButton(text="Вернуться назад", callback_data="back_button")
    markupBack.add(back_button)
    bot.edit_message_media(
        media=InputMediaPhoto(profil), chat_id=call.message.chat.id, message_id=call.message.message_id
    )
    bot.edit_message_caption(
        chat_id=call.message.chat.id,
        message_id=call.message.id,
        caption="🩸Приглашай больше путников в канал @strashnoe\_delo, используя свою личную ссылку\-приглашение\n\n🩸За каждого Новобранца ты будешь получать 1 душу\n\n🩸Так же ты будешь получать 1 душу каждый раз, когда приглашенный тобой Искатель сделает татуировку у @strashnoe\_delo\n"
        + f"\nТвоя реферальная ссылка:\n{link}\n\nКоличество Новобранцев: {ref}\nКоличесто твоих Душ : {souls}",
        parse_mode="MarkdownV2",
        reply_markup=markupBack,
    )


def back(call):
    bot = importlib.import_module("main.bot")
    
    markupWork = InlineKeyboardMarkup()
    work_button = InlineKeyboardButton(text="Мой профиль", callback_data="work_button")
    run_button = InlineKeyboardButton(text="Бежать в страхе", callback_data="run_button")
    markupWork.add(work_button, run_button)
    bot.edit_message_media(
        media=InputMediaPhoto(page2), chat_id=call.message.chat.id, message_id=call.message.message_id
    )
    bot.edit_message_caption(
        chat_id=call.message.chat.id,
        message_id=call.message.id,
        caption="Поздравляю, отныне ты Искатель душ! Реферальную ссылку ты сможешь найти в своём профиле",
        reply_markup=markupWork,
    )


def check(call):
    bot = importlib.import_module("main.bot")
    
    status = ["creator", "adminstrator", "member"]
    if bot.get_chat_member(chat_id=CHANNEL_ID, user_id=call.message.chat.id).status in status:
        back(call)
    else:
        bot.send_message(call.message.chat.id, "Подпишитесь на канал !")
