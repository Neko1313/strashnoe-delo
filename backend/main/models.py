from django.db import models
from django.utils.translation import gettext_lazy as _


class Item(models.Model):
    class Meta:
        verbose_name = _("Товар для продащи")
        verbose_name_plural = _("Товары для продажи")

    price = models.IntegerField(verbose_name=_("Цена"), help_text=_("Цена в душах"))
    info = models.TextField(verbose_name=_("Информация"), help_text=_("Текст описания товара"))
    name = models.CharField(max_length=30, verbose_name=_("Название"), help_text=_("Название товара"))
    img = models.ImageField(
        upload_to="img_item", verbose_name=_("Изображение"), help_text=_("Фото которое будет отображаться в карточке")
    )
    is_limit = models.BooleanField(
        verbose_name=_("Ограниченое количество"), help_text=_("Если сторит галочка то есть орграничение")
    )
    limit = models.IntegerField(
        null=True, verbose_name=_("Ограничение"), help_text=_("Если стоит галочка введите число")
    )

    def __str__(self):
        return self.name


class UserSD(models.Model):
    class Meta:
        verbose_name = _("Пользователь бота")
        verbose_name_plural = _("Пользователи бота")

    userid = models.IntegerField(verbose_name=_("Чат id пользователя"))
    refid = models.CharField(null=True, max_length=30, verbose_name=_("Чат id пользователя который пригласил"))
    souls = models.IntegerField(verbose_name=_("Количество душ"))
    ref = models.IntegerField(verbose_name=_("Количество рефиралов"))
    name_user = models.TextField(verbose_name=_("Имя пользователя для бота"))
    name_user_tg = models.TextField(null=True, verbose_name=_("Имя пользователя в телеграм"))

    def __str__(self):
        return f"{self.userid}"


class ItemsUser(models.Model):
    class Meta:
        verbose_name = _("Товар купленый пользователем")
        verbose_name_plural = _("Товары купленныме полезователями")

    userid = models.ForeignKey("UserSD", on_delete=models.PROTECT)
    itemid = models.ForeignKey("Item", on_delete=models.PROTECT)

    def __str__(self):
        return f"{self.userid.userid} {self.itemid.name}"
