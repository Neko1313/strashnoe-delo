#!/bin/bash

python manage.py makemigrations
# Выполнение миграций
python manage.py migrate --noinput

# Здесь можно добавить команды для создания суперпользователя или инициализации базы данных
# python manage.py createsuperuser --noinput --username=admin --email=admin@example.com

# Запуск сервера Django
python manage.py runserver 0.0.0.0:8000
